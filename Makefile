
.PHONY: dist install clean

default: dist

dist: build/setup.py build/wesnothlexer/__init__.py build/wesnothlexer/wesnoth.py
	cd build && ./setup.py bdist_egg

install: build/setup.py build/wesnothlexer/__init__.py build/wesnothlexer/wesnoth.py
	cd build && ./setup.py install

clean:
	rm -rf build

build/wesnothlexer:
	mkdir -p build/wesnothlexer

build/setup.py: setup.py | build/wesnothlexer
	cp -f setup.py build/setup.py

build/wesnothlexer/wesnoth.py: wesnoth.py | build/wesnothlexer
	cp -f wesnoth.py build/wesnothlexer/wesnoth.py

build/wesnothlexer/__init__.py: | build/wesnothlexer
	touch build/wesnothlexer/__init__.py

