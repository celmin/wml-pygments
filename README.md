
WML Syntax Highlighting
=======================

This is a syntax highlighting module using the [Pygments](https://pygments.org) framework.
To install in your copy of Pygments, simply run `make install`.
A distributable egg can also be build with 	`make dist`.

If you just want to try it out without installing it in the Pygments directory,
you can do so. The wesnoth.py script can be called with two arguments, the input
file and the output file, in that order. In addition, the optional flags
`--wfl`, `--wml`, and `--fai` can be used to specify the language. (It doesn't
do proper options parsing, only checks if the first argument is an option, so
be sure to use only one flag.)
