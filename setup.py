#!/usr/bin/env python3

from setuptools import setup, find_packages

setup (
  name='wesnothlexer',
  packages=find_packages(),
  install_requires=["pygments >= 2.7"],
  entry_points =
  """
  [pygments.lexers]
  wmllexer = wesnothlexer.wesnoth:WmlLexer
  wfllexer = wesnothlexer.wesnoth:WflLexer
  failexer = wesnothlexer.wesnoth:FaiLexer
  """,
)
