#!/usr/bin/env python3
# A Pygments lexer for Wesnoth's domain-specific languages.
# Currently supports WML, WFL, and FAI (WFL with extra built-ins).
# To enable in your Pygments install, place this file in the
# lexers directory.

from pygments.lexer import RegexLexer
from pygments.lexer import bygroups, words, include, using, this, combined, default
from pygments.lexers.scripting import LuaLexer
import pygments.token as T
import re

"""

List of token types used by LuaLexer:
- Comment.Preproc (only used for hashbang, so can be considered irrelevant here)
- Comment.Multiline
- Comment.Single
- Number.Float
- Number.Hex
- Number.Integer
- Text
- String
- Operator
- Punctuation
- Operator.Word
- Keyword
- Keyword.Declaration
- Keyword.Constant
- Name
- String.Single
- String.Double
- Name.Class
- Name.Function
- String.Escape

"""

__all__ = ['WflLexer', 'FaiLexer', 'WmlLexer']

# My custom token names
# These are here so that I can easily shuffle around the actual token types
# (or even replace them with custom ones) without altering the parser code (much).
class WFL:
	Keyword = T.Keyword
	Name = T.Name.Function
	Builtin = T.Name.Builtin
	Operator = T.Operator
	# Also used: Operator.Word
	Punctuation = T.Punctuation
	String = T.String
	# Also used: String.Escape
	Comment = T.Comment

class FAI:
	Builtin = T.Name.Builtin

class WML:
	Boolean = T.Keyword.Constant
	# Macro stuff
	class Macro:
		File = T.Comment.PreprocFile
		Include = T.Comment.Multiline
	Tag = T.Name.Tag
	# Subtokens: Event, Animation, Insert
	Attribute = T.Name.Attribute
	Number = T.Number
	String = T.String
	Variable = T.String.Interpol
	Preproc = T.Comment.Preproc
	Comment = T.Comment
	# Also used: Comment.Special
	Punct = T.Punctuation

# Register CSS classes for the custom token types
T.STANDARD_TYPES.update({
	WML.Tag.Event:		'nte',
	WML.Tag.Animation:	'nta',
	WML.Tag.Insert:		'nti',
})

# Might move this class to a separate file eventually
from pygments.style import Style
class WmlStyle(Style):
	default_style = ""
	styles = {
		T.Keyword:				"bold #808",
		T.Keyword.Pseudo:		"nobold #03e",
		T.Operator:				"bold",
		T.Operator.Word:		"#088",
		T.Punctuation:			"bold #44f",
		T.Name:					"#00f",
		T.Name.Builtin:			"#08f",
		T.Name.Tag:				"bold",
		T.Name.Tag.Event:		"bold #bb0",
		T.Name.Tag.Insert:		"#03e",
		#T.Name.Tag.Animation:	"",
		T.String:				"#f00",
		T.String.Interpol:		"italic #03e",
		T.String.Escape:		"#033",
		T.String.Doc:			"#330",
		T.Number:				"#d60 bold",
		T.Comment:				"#082",
		T.Comment.Preproc:		"bold #800",
		T.Comment.PreprocFile:	"italic #880",
		T.Comment.Special:		"bold",
		T.Comment.Multiline:	"#880",
		T.Error:				"bg:#530 #f00 bold",
	}

"""
	
	def __init__(self, *args, **kwargs):
		super(type(self), self).__init__(*args, **kwargs)
"""

class WflLexer(RegexLexer):
	bracket_stack = []
	bracket_matches = {']': '[', ')': '('}
	builtins = {
		# Conditionals
		'if', 'switch',
		# Numeric
		'abs', 'acos', 'as_decimal', 'asin', 'atan',
		'cbrt', 'ceil', 'cos', 'exp', 'floor', 'hypot',
		'log', 'max', 'min', 'pi', 'root', 'round',
		'sin', 'sqrt', 'sum', 'tan', 'wave',
		# Strings
		'concatenate', 'contains_string', 'find_string',
		'length', 'replace', 'reverse', 'substring',
		# Lists & Maps
		'head', 'index_of', 'keys',
		'size', 'sort', 'tail', 'tolist', 'tomap', 'values',
		# Higher-Order
		'choose', 'filter', 'find',
		'map', 'reduce', 'take_while', 'zip',
		# Misc
		'distance_between', 'loc', 'null', 'type',
		# Debug
		'debug', 'debug_print', 'debug_float',
		'dir', 'refcount',
	}
	name = 'WFL'
	aliases = ['wfl']
	filenames = ['*.wfl']
	encoding = 'utf-8'
	tabsize = 4
	
	def push_brace(self, match):
		brace = match.group(1)
		self.bracket_stack.append(brace)
		yield match.start(1), WML.Macro.Include, brace
	
	def pop_brace(self, match):
		brace = match.group(0)
		tok_type = T.Error
		if self.bracket_stack[-1] == self.bracket_matches[brace]:
			self.bracket_stack.pop()
			tok_type = WML.Macro.Include
		yield match.start(0), tok_type, brace
	
	def function_call(self, match):
		if match.group(1) in self.builtins:
			yield match.start(1), WFL.Builtin, match.group(1)
		else:
			yield match.start(1), WFL.Name, match.group(1)
		yield match.start(2), T.Whitespace, match.group(2)
		yield match.start(3), WFL.Punctuation, match.group(3)
	
	tokens = {
		'root': [
			(r'\s+', T.Whitespace),
			("(wfl|fai)(\s*)(')", bygroups(WFL.Keyword, T.Whitespace, WFL.String), 'str'),
			(
				r'(def)(\s+)([a-zA-Z_]+)(\s*)(\()',
				bygroups(WFL.Keyword, T.Whitespace, T.Text, T.Whitespace, WFL.Punctuation),
				('fcn','params')
			),
			default('base'),
		],
		'base': [
			(r'\s+', T.Whitespace),
			('(?:wfl|fai)end', WFL.Keyword, '#pop'),
			(
				words(('and','or','not', 'd', 'in'), suffix='(?=[^a-zA-Z])'),
				WFL.Operator.Word
			),
			('where', WFL.Keyword),
			(r'([a-zA-Z_]+)(\s*)(\()', function_call, '#push'),
			(r'[a-zA-Z_]+', T.Text),
			# These two could perhaps be moved to a delegating lexer setup
			(r'\{\S.*\}', WML.Macro.Include), # WML preprocessor macros (simplified)
			(r'\$[a-zA-Z0-9_.[\]]+\|?', WML.Variable), # WML variable subst (simplified)
			#(r'\$[a-zA-Z0-9_.[\]]+(\|?|\?.*\|)', T.Other), # WML variable subst (simplified)
			(r'#[^#]*#', WFL.Comment),
			(r'[([]', WFL.Punctuation, '#push'),
			(r'[)\]]', WFL.Punctuation, '#pop'),
			(r'[0-9]+(\.[0-9]+)?', T.Number),
			("'", WFL.String, 'str'),
			(',', WFL.Punctuation),
			(r'[-\<=>+*/%^.~]|!=|<=|>=|\.\.', WFL.Operator),
		],
		'params': [
			(r'\s+', T.Whitespace),
			('[a-zA-Z_]+', T.Text),
			('[*,]', WFL.Punctuation),
			(r'\)', WFL.Punctuation, '#pop'),
		],
		'fcn': [
			(';', WFL.Punctuation, '#pop'),
			('[^;]+', using(this, state='base')),
		],
		'str': [
			(r"\['\]", WFL.String.Escape),
			(r"\[[(')]\]", WFL.String.Escape),
			("'", WFL.String, '#pop'),
			(r"[^'[]+", WFL.String),
			(r'\[', WFL.String.Interpol, 'interpol'),
		],
		'interpol': [
			(r'\]', WFL.String.Interpol, '#pop'),
			include('base'),
		],
	}

class FaiLexer(WflLexer):
	name = 'FAI'
	aliases = ['fai']
	filenames = ['*.fai']
	encoding = 'utf-8'
	tabsize = 4
	
	ai_builtins = {
		# Base
		'attack', 'fallback', 'get_unit_type', 'move', 'move_partial',
		'run_file', 'set_unit_var', 'set_var',
		# Evaluation
		'calculate_outcome', 'chance to hit', 'evaluate_for_position',
		'max_possible_damage', 'max_possible_damage_with_retaliation',
		'movement_cost', 'next_hop', 'outcomes', 'timeofday_modifier',
		# Game Map
		'adjacent_locs', 'castle_locs', 'close enemies',
		'distance_to_nearest_unowned_village', 'defense_on', 'find_shroud',
		'is_avoided_location', 'is_unowned_village', 'is_village', 'shortest_path',
		'simplest_path', 'suitable_keep', 'unit_at', 'nearest_keep',
		'nearest_loc', 'unit_moves', 'units_can_reach',
		# Recruitment
		'recruit',
	}
	
	def get_tokens_unprocessed(self, text, **kwargs):
		for index, token, value in WflLexer.get_tokens_unprocessed(self, text, **kwargs):
			if token is WFL.Name and value in self.ai_builtins:
				yield index, FAI.Builtin, value
			else:
				yield index, token, value

class Formula:
	STR, WFL, FAI, ANIM = range(4)

class WmlLexer(RegexLexer):
	bracket_stack = []
	bracket_matches = {'}': '{', ')': '('}
	tag_stack = []
	current_attr = ''
	in_animation = False
	
	name = 'WML'
	aliases = ['wml', 'Wml']
	filenames = ['*.cfg', '*.pbl']
	encoding = 'utf-8'
	tabsize = 4
	
	def push_brace(self, match):
		brace = match.group(1)
		self.bracket_stack.append(brace)
		yield match.start(1), WML.Macro.Include, brace#'--OPENING BRACE--' + brace
		if match.lastindex == 1:
			return # This happens if it was a parenthesis, not a brace
		name = match.group(2)
		tok_type = WML.Macro.Include
		if '/' in name: # Assume it's a filename
			tok_type = WML.Macro.File
		yield match.start(2), tok_type, name
	
	def pop_brace(self, match):
		brace = match.group(0)
		tok_type = T.Error
		if self.bracket_stack[-1] == self.bracket_matches[brace]:
			self.bracket_stack.pop()
			tok_type = WML.Macro.Include
		yield match.start(0), tok_type, brace# + '--CLOSING BRACE--'
	
	def wml_tag(self, match):
		name = match.group(1)
		tok_type = WML.Tag.Generic
		if name == 'event':
			tok_type = WML.Tag.Event
		elif name == 'animation' or name == 'death' or name == 'defend' or name.endswith('_anim'):
			tok_type = WML.Tag.Animation
			if match.group(0)[1] == '/':
				self.in_animation = False
			else:
				self.in_animation = True
		elif name == 'insert_tag':
			tok_type = WML.Tag.Insert
		if match.group(0)[1] == '/':
			if name == self.tag_stack[-1]:
				self.tag_stack.pop()
		else:
			self.tag_stack.append(name)
		yield match.start(0), tok_type, match.group(0)	
	
	def store_attr(self, match):
		# Doesn't really work when several attributes are set at once
		# However, the attributes we're really interested in probably won't
		# be combined like that.
		self.current_attr = match.group(1)
		start = 0
		for component in re.finditer(r'\s*,\s*', match.group(0)[:-1]):
			yield match.start(0) + start, WML.Attribute, match.group(0)[start : component.start(0)]
			yield component.start(0), WML.Punct, component.group(0)
			start = component.end(0)
		yield match.start(0) + start, WML.Attribute, match.group(0)[start:-1]
		yield match.end(0) - 1, WML.Punct, match.group(0)[-1]
	
	def parse_str(self, match):
		# First, check if we should be considering this to be a formula
		cur_tag = self.tag_stack[-1] if len(self.tag_stack) else ''
		cur_attr = self.current_attr
		str_type = Formula.STR
		if cur_tag == "vars":
			str_type = Formula.FAI
		elif cur_attr == "formula":
			str_type = Formula.FAI if cur_tag == 'ai' else Formula.WFL
		elif cur_tag == "candidate_action" and cur_attr in ("evaluation", "action"):
			str_type = Formula.FAI
		elif cur_tag == "stage" and cur_attr == "move":
			str_type = Formula.FAI
		elif cur_tag == "filter" and cur_attr in ("me", "target"):
			str_type = Formula.FAI
		elif cur_tag == "ai" and cur_attr in ("priority", "loop_formula"):
			str_type = Formula.FAI
		elif self.in_animation:
			str_type = Formula.ANIM
		# TODO: GUI2 and movetype patching formulas (string starts and ends with paren)
		if str_type == Formula.STR:
			token_stream = using(this, state='quoted')
		elif str_type == Formula.WFL:
			token_stream = using(WflLexer)
		elif str_type == Formula.FAI:
			token_stream = using(FaiLexer)
		elif str_type == Formula.ANIM:
			token_stream = using(this, state='animation')
		for tok in token_stream(self, match):
			yield tok
	
	tokens = {
		'root': [
			( # Consume any whitespace first
				r'\s+', T.Whitespace
			), ( # Preprocessor directives
				words((
					'ifdef', 'ifndef', 'ifhave', 'ifnhave', 'ifver', 'ifnver',
					'else', 'endif', 'undef',
					'error', 'warning', 'textdomain', 'deprecated'
				), prefix='#', suffix=r'.*$'),
				WML.Preproc,
			), ( # Special processing instructions
				words((
					'wmllint', 'wmlscope', 'wmlindent', 'wmlxgettext', 'po', 'trackplacer'
				), prefix=r'(#\s*)(', suffix=r':)'),
				bygroups(WML.Comment, WML.Comment.Special),
				'comment'
			),
			# Define and enddef have special-case handling
			(r'#define.*(?=#|$)', WML.Preproc, 'define'),
			# For some reason, the main enddef rule sometimes misses the closing enddef
			(r'#enddef', WML.Preproc), # Catch mismatched or missed enddefs
			# Comments
			(r'#(.*)$', WML.Comment),
			# Tags
			(r'\[[/+]?([a-zA-Z0-9_]+)\]', wml_tag),
			( # Attributes
				r'([a-zA-Z0-9_]+)(\s*,\s*[a-zA-Z0-9_]+)*\s*=',
				store_attr, # WML.Attribute
				'value'
			),
			include('macro_inc'),
		],
		'define': [
			(r'#enddef', WML.Preproc, '#pop'),
			(r'#arg.*(?=#|$)', WML.Preproc, 'macro_opt_arg'),
			(r'#endarg', WML.Preproc),
			include('root'),
			include('value'),
		],
		'macro_opt_arg': [
			(r'#endarg', WML.Preproc, '#pop'),
			include('root'),
			include('value'),
		],
		'macro_inc': [
			# Macro substitutions, with either macro name or filepath
			(r'(\{)([^\s}]+)', push_brace, 'macro'),
		],
		'macro': [
			(r'\s+', T.Whitespace),
			# Nested macro substitution
			include('macro_inc'),
			# Closing brace
			(r'\}', pop_brace, '#pop'),
			# String argument (allows spaces within it)
			(r'(?:_)?"([^"]|"")"', using(this, state='value')),
			## Key-value argument
			(r'[^(]\S*=[^}\s]+', using(this)),
			# Other argument
			(r'[^\s(}]+', using(this, state='value')),
			# Basic parenthesized (allows spaces and newlines, but no = or tags)
			(
				r'(\()([^=\][)]+)(\))',
				bygroups(
					WML.Macro.Include,
					using(this, state='value'),
					WML.Macro.Include
				)
			),
			# Full parenthesized (allows spaces, newlines, tags, etc)
			(
				r'(\()([^)]*)(\))',
				bygroups(
					WML.Macro.Include,
					using(this),
					WML.Macro.Include
				),
			),
		],
		'comment': [
			# The rest of a special marker comment
			(r'(.*?)$', WML.Comment, '#pop'),
		],
		'value': [
			(r'[ \t]+', T.Whitespace),
			(',', WML.Punct),
			# Numbers
			(r'-?[0-9]+(?=[,#+]|$)', WML.Number),
			# Booleans
			(
				words(('true','false','yes','no'), suffix='(?=\s*(?:#|$))'),
				WML.Boolean
			),
			# Quoted Strings
			(r'(_\s*")([^^"{$]*\^)', bygroups(WML.String, WML.String.Doc), 'quoted_proxy'),
			(r'(?:_\s*)?"', WML.String, 'quoted_proxy'),
			# Translatable Angled Strings
			# (Assumed to be input for the CFG name generator)
			(r'(_\s*<<)', WML.String, 'cfg_def'),
			# Angled Strings
			(
				r'(<<)((?:[^>]|>[^>])*?)(>>)',
				bygroups(WML.String, using(LuaLexer), WML.String)
			),
			include('macro_inc'),
			include('unquoted'),
			# End-of-line Comments
			(r'#(.*)$', WML.Comment, '#pop'),
			# But if there was a + just prior, stay in value mode
			(
				r'(\+)(\s*)(#(?:.*)$)([\n\r]*)',
				bygroups(WML.Punct, T.Whitespace, WML.Comment, T.Whitespace)
			),
			# Consume + as well, which may be followed by a newline
			(r'(\+)(\s*)', bygroups(WML.Punct, T.Whitespace)),
		],
		'quoted_proxy': [
			(r'([^"]|"")+', parse_str),
			# Closing quote without + operator
			(r'"', WML.String, '#pop'),
		],
		'quoted': [
			# String content, including possibly escaped quotes
			(r'([^"{$]|"")+', WML.String),
			# Formula Substitutions
			(r'(\$\()((?:[^"$]|\$[^(])*)(\))', bygroups(WML.Variable, using(WflLexer), WML.Variable)),
			# Variable substitutions
			(r'\$', WML.Variable, 'variable'),
			include('macro_inc'),
			default('#pop'),
		],
		'cfg_def': [
			# Nonterminal name
			(r'(\n\s*)([^=}]+)(=)', bygroups(T.Whitespace, WML.Attribute, WML.Punct)),
			# Nonterminal substitution
			(r'\{[^=}]+?\}', WML.Macro.Include),
			# End of definition
			('>>', WML.String, '#pop'),
			# Separator
			(r'\|', WML.Punct),
			# Other
			(r'[^{}=>|]+|>[^{}=>|]', WML.String),
		],
		'unquoted': [
			# Unquoted Strings
			(r'([^\s$#,+"]+)', WML.String),
			# Unquoted Formula Substitutions
			# Unfortunately, this will fail if there's a line break in the formula
			# This is not easily solvable and probably not worth solving,
			# as quoting of formulas is strongly recommended anyway.
			(r'(\$\()((?:[^+\r\n$]|\$[^(])*)(\))', bygroups(WML.Variable, using(WflLexer), WML.Variable)),
			# Unquoted Variable Substitution
			(r'\$', WML.Variable, 'variable'),
		],
		'variable': [
			# Basic variable name
			(r'[a-zA-Z0-9_]+', WML.Variable),
			# Dot access into variable
			(r'\.', WML.Variable),
			# Indexed access into variable, with literal number
			(r'(\[)(\d+)(\])', bygroups(WML.Variable, WML.Number, WML.Variable)),
			# Indexed access into variable, using variable index
			(r'\[\$', WML.Variable, 'index_variable'),
			# Nested variable substitution
			(r'\$', WML.Variable, 'variable'),
			# Default value for variable substitution
			(r'\?', WML.Variable, 'variable_default'),
			# End of variable substitution, either explicitly or implicitly
			(r'\||(?=[^a-zA-Z0-9_])', WML.Variable, '#pop'),
		],
		'index_variable': [
			(r'\]', WML.Variable, '#pop'),
			include('variable'),
		],
		'variable_default': [
			include('unquoted'),
			(r'\|', WML.Variable, '#pop:2'),
		],
		'animation': [
			(r'\[', WML.Punct, 'animation_subst'),
			(r'[^[:]', WML.String),
			(r'(:)(\d+)', bygroups(WML.Punct, WML.Number)),
			(':', WML.String),
		],
		'animation_subst': [
			(r'\]', WML.Punct, '#pop'),
			(r'(\*)(\d+)', bygroups(WML.Punct, WML.Number)),
			(r'(\d+)(~)(\d+)', bygroups(WML.Number, WML.Punct, WML.Number)),
			(',', WML.Punct),
			(r'[^~*,\]]', WML.String),
		],
	}

# Possible future expansions:
# - Highlight Pango markup in strings
# - Highlight HTML entities in strings
# - Highlight IPF in strings
# - Highlight Wesnoth API functions in Lua
# - Highlight Wesnoth AI API functions in Lua
# Some of the above might want a way to know which tag is currently being parsed.
# (In particular the animation syntax.)

if __name__ == '__main__':
	lang_args = ('--wfl', '--fai', '--wml')
	from pygments import highlight
	from pygments.formatters import HtmlFormatter
	import sys, codecs
	
	if sys.argv[1] in lang_args:
		lexer = globals()[__all__[lang_args.index(sys.argv[1])]]()
		sys.argv.pop(1)
	else:
		lexer = WmlLexer()
	
	fmt = HtmlFormatter(style=WmlStyle)
	
	with codecs.open(sys.argv[1], encoding='utf-8') as infile:
		with codecs.open(sys.argv[2], encoding='utf-8', mode='w') as outfile:
			print("<!DOCTYPE html><html><head><style>", file=outfile)
			print(fmt.get_style_defs(), file=outfile)
			print("</style></head></body>", file=outfile)
			highlight(infile.read(), lexer, fmt, outfile=outfile)
			print("</body></html>", file=outfile)
